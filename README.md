# README #

Interaktywa konsola C++. Do ćwiczeń i nauki języka.

    ************************************************************************
    *    Interaktywa konsola C++ i C. Do ćwiczeń i nauki języka.
    *
    * Biblioteki zaimportowane :  #include <iostream> <string> <cmath>
    * Kompilator:                 g++
    * Standart :                  C++ -std=c++11, C -std=c98
    ************************************************************************

    FUNKCJE KONSOLI:
    Koniec Programu (slowa klucze) 'koniec()', 'nara()', 'end()', 'quit()'
    info(include) - to co mamy aktualnie zaimportowane
    run()         - ponowne uruchomienie skompilowanego programu z sukcesem

    ************************************************************************

    Gdy program zostanie uruchomiony z opcją ./gcpp --c Konsola będzie pracować
     w języku C a nie c++ .W którym języku pracujesz ?
     Możesz sprawdzić przez info(include).

### What is this repository for? ###

Do nauki języka C++ i C w konsoli. Cały kod jest tworzony i wpisany w funkcje int main().
 Narazie nie można rozbudowywać programu o dodatkowe funkcje w konsoli.

### How do I get set up? ###

Kompilator g++ 
Standart C++ std=c++11 , lub dla C std=c98
Python3



### Who do I talk to? ###

pracacp@gmail.com
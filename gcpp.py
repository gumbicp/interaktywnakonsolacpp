#!/usr/bin/env python3
# -*- coding: utf-8 -*
#
# Copyright 2014 gumbi <pracacp@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
__author__ = "gumbicp"
__email__ = "pracacp@gmail.com"
__version__ = 'v 0.0.1'

"""
Interaktywa konsola C++ v 0.0.1
"""

import readline
import sys
import os
from kompilator import *


class Konsola(object):
    """
    ************************************************************************
    *    Interaktywa konsola C++ i C. Do ćwiczeń i nauki języka.
    *
    * Biblioteki zaimportowane :  #include <iostream> <string> <cmath>
    * Kompilator:                 g++
    * Standart :                  C++ -std=c++11, C -std=c98
    ************************************************************************

    FUNKCJE KONSOLI:
    Koniec Programu (slowa klucze) 'koniec()', 'nara()', 'end()', 'quit()'
    info(include) - to co mamy aktualnie zaimportowane
    run()         - ponowne uruchomienie skompilowanego programu z sukcesem
    change_lang() - zmiana języka z C++ na C lub C na C++.

    ************************************************************************

    Gdy program zostanie uruchomiony z opcją ./gcpp --c Konsola będzie pracować
     w języku C a nie c++ .W którym języku pracujesz ?
     Możesz sprawdzić przez info(include).
    """
    def __init__(self):
        self.historia_plik = os.path.expanduser("~/.myconsole-history")
        self.komenda = []
        self.flaga_koniec = False
        self.f_run = False
        self._info()

    def _info(self):
        """
        Metoda czyści ekran i pokazuje czołówkę informacyjną o programie.
        """
        subprocess.call(['clear'])
        print(self.__doc__)
        print('Author : ', __author__)
        print('Email : ', __email__)
        print('Version: ', __version__, '\n')

    def _wejscie_komend(self, obj_kompilator):
        """

        :param obj_kompilator: Obiekt klasy Kompilator
        :return:
        """
        instrukcje = []
        try:
            sys.ps1
        except AttributeError:
            sys.ps1 = ">>> "
        try:
            sys.ps2
        except AttributeError:
            sys.ps2 = "... "

        znak_zachety = sys.ps1
        tmp_str = input(znak_zachety)

        licznik = 0
        while True:
            znak_zachety = sys.ps2

            if tmp_str == 'run()':
                Kompilator.bez_kompilacji()
                self.f_run = True
                break

            if tmp_str == 'change_lang()':
                if obj_kompilator.f_c_lang:
                    obj_kompilator.set_cpp_lang()
                    print("info :: C++ ustawiony...")
                    break
                else:
                    obj_kompilator.set_clang()
                    print("info :: C ustawiony...")
                    break

            if tmp_str in ('koniec()', 'nara()', 'end()', 'quit()'):
                self.flaga_koniec = True
                break

            if not tmp_str:
                licznik += 1
                if licznik == 2:
                    break

            tmp_str = tmp_str.strip()

            if tmp_str.startswith("#include"):
                tmp_str = tmp_str[10:]
                tmp_str = tmp_str[:-1]
                if not tmp_str in obj_kompilator.includes:
                    obj_kompilator.includes.append(tmp_str)
                else:
                    print("info :: Biblioteka ", tmp_str, " była już importowana do konsoli")
                tmp_str = input(znak_zachety)

            elif tmp_str == 'info(include)':

                for incl in obj_kompilator.include[1:]:
                    print('info :: #include <', incl, '>')

                for incl in obj_kompilator.includes:
                    print('info :: #include <', incl, '>')

                break
            else:
                instrukcje.append(tmp_str)
                self.komenda.append(tmp_str)
                tmp_str = input(znak_zachety)

        return instrukcje

    def pentla_programu(self, arg):
        """
        Główna petla programu.
        - Tworzy obiekt kompilatora
            dla kodu C++
        -
        :return:
        """
        obj_kompilator = Kompilator()
        if arg.c:
            obj_kompilator.set_clang()

        while True:
            tab_instrukcji = self._wejscie_komend(obj_kompilator)

            if self.flaga_koniec:
                break
            #sterowanie ponownym wykonaniem programu bez kompilacji
            if not self.f_run:
                obj_kompilator.kompiluj_main(tab_instrukcji)
            else:
                self.f_run = False


def argparser():
    import argparse

    args_pars = argparse.ArgumentParser(description="Interactive C and C++ console.")
    args_pars.add_argument('--c',
                           action='store_true',
                           default=False,
                           help='Console for C language')
    arg = args_pars.parse_args()
    ob_start = Konsola()
    ob_start.pentla_programu(arg)


if __name__ == '__main__':
    argparser()

#TODO: opisy
#TODO: namespace
#TODO: pamięć zmiennych
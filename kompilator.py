#!/usr/bin/env python3
# -*- coding: utf-8 -*
#
# Copyright 2014 gumbi <pracacp@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
import subprocess
from xml.etree.ElementTree import parse


DEBUG = False
kompilator = 'g++'
std_c = '-std=c++11'
p_main = 'main.cpp'
p_szablon = 'main.xml'
P_OUT = ['./a.out']
END_L = '\n'


class Kompilator(object):

    def set_clang(self):
        """
        Metoda zmienia język konsoli na C iso98
        :return:
        """
        global p_main
        global std_c
        p_main = 'main.c'
        std_c = '-std=c98'
        self.f_c_lang = True
        self._czysc_zmienne()

    def set_cpp_lang(self):
        """
        Metoda zmienia język konsoli na C++ c11
        :return:
        """
        global p_main
        global std_c
        p_main = 'main.cpp'
        std_c = '-std=c++11'
        self.f_c_lang = False
        self._czysc_zmienne()

    def _czysc_zmienne(self):
        """
        Metoda czyści zmienne.
        """
        if not self.f_c_lang:
            self.doc = parse('main.xml')
        else:
            self.doc = parse('mainc.xml')
        self.plik_main = None
        self.kod_pliku = []
        self.include = []
        self.f_nazwa = ""
        self.f_args = []
        self.f_instrukcje = []
        self.f_typ = ''
        self.tab_komp_main = [kompilator, p_main]
        self._process_main()

    def __init__(self):
        self.f_c_lang = False
        self.doc = parse('main.xml')
        self.plik_main = None
        self.kod_pliku = []
        self.include = []
        self.namespace = []
        self.includes = []
        self.f_nazwa = ""
        self.f_args = []
        self.f_instrukcje = []
        self.f_typ = ''
        self.tab_komp_main = [kompilator, std_c, p_main]
        self._process_main()

    def _otworz_main(self):
        """
        Metoda otwiera plik p_main (main.cpp) standardowo
        """
        try:
            self.plik_main = open(p_main, 'w')
        except IOError as e:
            print(e)

    def _zamknij_main(self):
        """
        Metoda zamyka plik main.cpp
        """
        self.plik_main.close()

    def dodaj_includes(self, include):
        """
        Metoda dodaje do tablicy importów biblioteki
         zaimportowane w sesji konsoli.
         Dzięki temu konsola pamięta co importowaliśmy.

        :include: string np cmath
        """
        self.includes.append(include)

    def kompiluj_main(self, instrukcje):
        self._otworz_main()
        self._dodaj_instrukcje(instrukcje)
        self._zapisz_main()
        self._zamknij_main()
        self._wykonaj_main()
        self._czysc_zmienne()

    def _dodaj_instrukcje(self, instrukcja):
        """
        Metoda dodaje instrukcja po instrukcji do kodu metody
        -- int main(){}
        :instrukcja: lista instrukcji kodu
        """
        self.f_instrukcje[:] = []
        for v in instrukcja:
            self.f_instrukcje.append(v)
        self.f_instrukcje.append('return 0;')

    def _zapisz_main(self):
        """
        Metoda zapisuje gotowy plik main.cpp do kompilacji
        """
        if len(self.includes):
            for incl in self.includes:
                self.include.append(incl)

        for v in self.include[1:]:
            self.kod_pliku.append(self.include[0])
            self.kod_pliku.append(' ')
            self.kod_pliku.append('<')
            self.kod_pliku.append(v)
            self.kod_pliku.append('>')
            self.kod_pliku.append(END_L)

        self.kod_pliku.append(END_L)
        self.kod_pliku.append(END_L)

        self.kod_pliku.append(self.f_typ)
        self.kod_pliku.append(' ')
        self.kod_pliku.append(self.f_nazwa)
        self.kod_pliku.append('(')

        for v in self.f_args:
            self.kod_pliku.append(v)
            self.kod_pliku.append(', ')
        self.kod_pliku.pop()
        self.kod_pliku.append('){')
        self.kod_pliku.append(END_L)
        self.kod_pliku.append(END_L)

        for v in self.f_instrukcje:
            self.kod_pliku.append('\t')
            self.kod_pliku.append(v)
            self.kod_pliku.append(END_L)
        self.kod_pliku.append(END_L)
        self.kod_pliku.append('}')
        self.kod_pliku.append(END_L)

        for v in self.kod_pliku:
            self.plik_main.write(v)

    def _wykonaj_main(self):
        """
        Methoda najpierw kompiluje kod do programu wynikowego,
        i jeżeli nie ma w nim błędów wykonuje go od razu.
        """
        try:
            retcode = subprocess.call(self.tab_komp_main)
            if retcode == 0:
                subprocess.call(P_OUT)

        except subprocess.SubprocessError as e:
            print(e)

    @staticmethod
    def bez_kompilacji():
        """
        Metoda uruchamia już skompilowany kod ponownie.
        """
        try:
            subprocess.call(P_OUT)
        except subprocess.SubprocessError as e:
            print(e)

    def _process_main(self):
        """
        Funkcja otwiera szablon main.xml dla pliku z funkcją main.
        """

        for item in self.doc.iterfind('main/include'):
            self.include.append(item.text)
            for i in item.findall('in'):
                self.include.append(i.text)

        for item in self.doc.iterfind('main/funkcja'):
            self.f_nazwa = item.findtext('nazw_funkcji')
            self.f_typ = item.findtext('typ')
            for i in item.iterfind('arg'):
                self.f_args.append(i.text)
            for i in item.iterfind('instrukcja'):
                self.f_instrukcje.append(i.text)
